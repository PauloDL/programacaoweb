<head>
<style type="text/css">

.login-form {
	width: 340px;
  margin: 50px auto;
}
.login-form form {
  margin-bottom: 15px;
  background: #f7f7f7;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  padding: 30px;
}
.login-form h2 {
  margin: 0 0 15px;
}
.form-control, .btn {
  min-height: 38px;
  border-radius: 2px;
}
.btn {
  font-size: 15px;
  font-weight: bold;
}
</style>
</head>
<body>
<?php
@session_start();
if(isset($_SESSION['logado'])){
$logado=$_SESSION['logado'];
}else {
   $logado=0;
}
if($logado==1){
      header("Location: ?pagina=menus");
}else {
?>
<br /><br /><br />
<div class="login-form">
    <form action="?pagina=controle" method="post">
        <h2 class="text-center">LOGAR</h2>
        <div class="form-group">
            <input type="text" class="form-control" name="usuario" placeholder="Usuario" required="required">
        </div>
        <div class="form-group">
            <input type="password" class="form-control"name="senha" placeholder="Senha" required="required">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">ENVIAR</button>
        </div>
    </form>
  </div>
<?php } ?>
</body>
</html>
