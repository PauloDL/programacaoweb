
ATEN�AO ONDE VOCE VE O XX TROQUE PELO O NUMERO DA SUA TURMA
EXEMPLO a turma da 353 troque loja_3XX por loja_353


CREATE DATABASE loja_3XX;




CREATE TABLE vestuario (
id INT PRIMARY KEY AUTO_INCREMENT,
categ_prod VARCHAR(50), tipo_prod VARCHAR(50), 
descri_prod TEXT, 
preco_prod FLOAT(10,2), 
foto_prod VARCHAR(50)
);


INSERT INTO vestuario (categ_prod, tipo_prod, descri_prod, preco_prod, foto_prod)
VALUES ("Camiseta", "Camiseta Adidas Core 18 - 1","Complete seu fardamento com a Camiseta Adidas Core 18 Masculina. Moderna e feita em material respir�vel, ela te deixa pronto para enfrentar partidas competitivas e emocionantes.",55.60,"Camiseta_adidas_C18.jpg");

INSERT INTO vestuario (categ_prod, tipo_prod, descri_prod, preco_prod, foto_prod)
VALUES ("Camiseta", "Camisa Adidas Squadra 17 - 2","Para voc� que n�o dispensa uma boa partida de futebol com os amigos, a Camisa Adidas Squadra 17 Masculina � perfeita para te acompanhar. Feita em Poli�ster, tem caimento bonito e muito confort�vel.",45.80,"Camiseta_adidas_Sq_17.jpg");

INSERT INTO vestuario (categ_prod, tipo_prod, descri_prod, preco_prod, foto_prod)
VALUES ("Camiseta", "Camiseta Adidas Entrada 18 - 3","A Camiseta Adidas Entrada 18 Masculina se destaca pelos detalhes de listras, uma marca registrada da Adidas. Al�m de estilosa, a pe�a � feita em tecido respir�vel e te deixa sempre pronto para o jogo.",44.50,"Camiseta_adidas_En_18.jpg");


INSERT INTO vestuario (categ_prod, tipo_prod, descri_prod, preco_prod, foto_prod)
VALUES ("Camiseta", "Camiseta Adidas Condivo 18 - 4","Pronta para vestir os boleiros de plant�o, a Camisa Masculina Condivo 18 Adidas possui um visual b�sico, e garante conforto t�rmico e total liberdade para sua movimenta��o nos gramados.",70.00,"Camiseta_adidas_Co_18.jpg");

INSERT INTO vestuario (categ_prod, tipo_prod, descri_prod, preco_prod, foto_prod)
VALUES ("Camiseta", "Camiseta Adidas Core 18 - 5","Complete seu fardamento com a Camiseta Adidas Core 18 Masculina. Moderna e feita em material respir�vel, ela te deixa pronto para enfrentar partidas competitivas e emocionantes.",55.60,"Camiseta_adidas_C18.jpg");

INSERT INTO vestuario (categ_prod, tipo_prod, descri_prod, preco_prod, foto_prod)
VALUES ("Camiseta", "Camisa Adidas Squadra 17 - 6","Para voc� que n�o dispensa uma boa partida de futebol com os amigos, a Camisa Adidas Squadra 17 Masculina � perfeita para te acompanhar. Feita em Poli�ster, tem caimento bonito e muito confort�vel.",45.80,"Camiseta_adidas_Sq_17.jpg");

INSERT INTO vestuario (categ_prod, tipo_prod, descri_prod, preco_prod, foto_prod)
VALUES ("Camiseta", "Camiseta Adidas Entrada 18 - 7","A Camiseta Adidas Entrada 18 Masculina se destaca pelos detalhes de listras, uma marca registrada da Adidas. Al�m de estilosa, a pe�a � feita em tecido respir�vel e te deixa sempre pronto para o jogo.",44.50,"Camiseta_adidas_En_18.jpg");

INSERT INTO vestuario (categ_prod, tipo_prod, descri_prod, preco_prod, foto_prod)
VALUES ("Camiseta", "Camiseta Adidas Core 18 - 8","Complete seu fardamento com a Camiseta Adidas Core 18 Masculina. Moderna e feita em material respir�vel, ela te deixa pronto para enfrentar partidas competitivas e emocionantes.",55.60,"Camiseta_adidas_C18.jpg");

INSERT INTO vestuario (categ_prod, tipo_prod, descri_prod, preco_prod, foto_prod)
VALUES ("Camiseta", "Camisa Adidas Squadra 17 - 9","Para voc� que n�o dispensa uma boa partida de futebol com os amigos, a Camisa Adidas Squadra 17 Masculina � perfeita para te acompanhar. Feita em Poli�ster, tem caimento bonito e muito confort�vel.",45.80,"Camiseta_adidas_Sq_17.jpg");

INSERT INTO vestuario (categ_prod, tipo_prod, descri_prod, preco_prod, foto_prod)
VALUES ("Camiseta", "Camiseta Adidas Entrada 18 - 10","A Camiseta Adidas Entrada 18 Masculina se destaca pelos detalhes de listras, uma marca registrada da Adidas. Al�m de estilosa, a pe�a � feita em tecido respir�vel e te deixa sempre pronto para o jogo.",44.50,"Camiseta_adidas_En_18.jpg");


INSERT INTO vestuario (categ_prod, tipo_prod, descri_prod, preco_prod, foto_prod)
VALUES ("Camiseta", "Camiseta Adidas Condivo 18 - 11","Pronta para vestir os boleiros de plant�o, a Camisa Masculina Condivo 18 Adidas possui um visual b�sico, e garante conforto t�rmico e total liberdade para sua movimenta��o nos gramados.",70.00,"Camiseta_adidas_Co_18.jpg");

INSERT INTO vestuario (categ_prod, tipo_prod, descri_prod, preco_prod, foto_prod)
VALUES ("Camiseta", "Camiseta Adidas Core 18 - 5","Complete seu fardamento com a Camiseta Adidas Core 18 Masculina. Moderna e feita em material respir�vel, ela te deixa pronto para enfrentar partidas competitivas e emocionantes.",55.60,"Camiseta_adidas_C18.jpg");

INSERT INTO vestuario (categ_prod, tipo_prod, descri_prod, preco_prod, foto_prod)
VALUES ("Camiseta", "Camisa Adidas Squadra 17 - 12","Para voc� que n�o dispensa uma boa partida de futebol com os amigos, a Camisa Adidas Squadra 17 Masculina � perfeita para te acompanhar. Feita em Poli�ster, tem caimento bonito e muito confort�vel.",45.80,"Camiseta_adidas_Sq_17.jpg");

INSERT INTO vestuario (categ_prod, tipo_prod, descri_prod, preco_prod, foto_prod)
VALUES ("Camiseta", "Camiseta Adidas Entrada 18 - 13","A Camiseta Adidas Entrada 18 Masculina se destaca pelos detalhes de listras, uma marca registrada da Adidas. Al�m de estilosa, a pe�a � feita em tecido respir�vel e te deixa sempre pronto para o jogo.",44.50,"Camiseta_adidas_En_18.jpg");