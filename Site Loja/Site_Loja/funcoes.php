﻿<?php
function conectar(){
define( 'MYSQL_HOST', 'localhost' );
define( 'MYSQL_USER', 'root' );
define( 'MYSQL_PASSWORD', '' );
define( 'MYSQL_DB_NAME', 'loja' );

try {
$conexao= new PDO( 'mysql:host=' . MYSQL_HOST . ';dbname=' . MYSQL_DB_NAME, MYSQL_USER, MYSQL_PASSWORD,
array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") );
} catch(PDOException $erro) {
    echo 'Mensagem de erro: ' . $erro->getMessage();
}
 return $conexao;
}
?>
