 <?php
  include_once("funcoes.php");
  $PDO = conectar();

  /*******
  Aqui vamos fazer o tratamento do dados e informações para que
  possamos fazer a paginação dos itens na pagina
  *******/
  //Se pg não existe atribui 1 a variável pg
  $itens_por_pagina=6;
  $pag = (isset($_GET['pag'])) ? (int)$_GET['pag'] : 1 ;
  $sql1= "SELECT * FROM vestuario where categ_prod='Camiseta'";
  $pesquisa1= $PDO->prepare($sql1);
  $pesquisa1->execute();
  $tot_reg = $pesquisa1->rowCount();
  $num_paginas = ceil($tot_reg/$itens_por_pagina);
  $inicio = ($pag * $itens_por_pagina) - $itens_por_pagina;
 /****************** FIM ***********************************/
 ?>
 <?php
  $itens_por_pagina=6;
  $sql = "SELECT * FROM vestuario where categ_prod='Camiseta' LIMIT
  $inicio,$itens_por_pagina";
  $pesquisa= $PDO->prepare($sql);
  $pesquisa->execute();
  ?>
  <div class="container py-5" id="camisetas">
  <div class="row">
  <?php
  $x=1;
  while($resultado = $pesquisa->fetch(PDO::FETCH_ASSOC)){
  ?>
  <div class="col-md-4">
  <div class="card fundo_card">
    <img src=<?php echo "img_produtos/",$resultado['foto_prod'] ?> class="card-img-top"/>
     <div class="card-body">
       <div class="card-title h5" style="text-align:center">
         <?php echo $resultado['tipo_prod']   ?>
       </div>
       <p class="card-text" style="text-indent:20px;text-align:justify">
        <?php echo $resultado['descri_prod'] ?>
        <hr />
        <p class="card-text" style="font-size:20px;color:blue;font-weight:bold;">
        <?php
          $valor=$resultado['preco_prod'];
          $valor = number_format($valor, 2, ',','.');
          echo "R$ ",$valor
        ?>
       </div>
     </div>
   </br>
  </div>
  <?php
   if($x % 3==0){
   echo "</div><br />  <div class='row'>";
  }
   $x++;
  }
  ?>
  </div>
  <nav>
  <ul class="pagination justify-content-center">
    <li class="page-item"><a class="page-link" href="?pagina=camiseta&pag=1">Primeiro</a></li>
  <?php for($i=1;$i<=$num_paginas;$i++){ ?>
    <li class="page-item"><a class="page-link" href="?pagina=camiseta&pag=<?php echo $i ?>">
    <?php echo  $i;?></a></li>
  <?php } ?>
    <li class="page-item"><a class="page-link" href="?pagina=camiseta&pag=<?php echo $num_paginas ?>">Último</a></li>
  </ul>
</nav>
