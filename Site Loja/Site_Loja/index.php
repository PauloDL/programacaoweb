<head>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="estilo.css" rel="stylesheet">
<meta charset="UTF-8">
<title>SITE LOJINHA </title>
<script src="bootstrap/js/jquery-3.3.1.min.js"></script>
<script>
  document.addEventListener("DOMContentLoaded", function(){
   var links = document.querySelectorAll(".navbar-nav li a:not([href='#'])");
   for(var x=0; x<links.length; x++){
   links[x].onclick = function(){
   $(".navbar-collapse").collapse('hide');
   }}});
</script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top " id="menu_principal">
  <div class="container">
    <a class="navbar-brand" href="?pagina=home">LOJINHA</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu_resp" >
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="menu_resp">
    <ul class="navbar-nav nav ml-auto">
      <li class="nav-item"><a href="?pagina=home" class="nav-link">HOME</a></li>
      <li class="nav-item dropdown">
      <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">VESTUÁRIOS</a>
      <div class="dropdown-menu bg-dark" id="submenu">
      <a href="?pagina=camiseta" class="dropdown-item" >CAMISETA</a>
      <a href="#" class="dropdown-item" >CALÇAS</a>
      <a href="#" class="dropdown-item" >JAQUETAS</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="#">ACESSÓRIOS</a>
    </div>
      </li>
      <li class="nav-item"><a href="#4" class="nav-link">CALÇADOS</a></li>
      <li class="nav-item"><a href="#5" class="nav-link">MATERIAL ESPORTIVO</a></li>
      <li class="nav-item"><a href="?pagina=login" class="nav-link">INTRANET</a></li>
      </ul>
      </div>
  </div>
</nav>
<?php
if(!isset($_GET['pagina'])){
  include('views/home.php');
}else {
  $pagina=$_GET['pagina'];
  switch ($pagina) {
    case 'home':
        include('views/home.php');
     break;
    case 'camiseta':
        include('views/camiseta.php');
     break;
    case 'login':
         include('views/login.php');
    break;
    case 'controle':
         include('views/controle.php');
    break;
    case 'menus':
         include('views/menus.php');
    break;
    case 'cadusuario':
         include('views/cad_usuario.php');
    break;
    case 'erro_login':
         include('views/erro_login.php');
    break;
    default:
      include('views/home.php');
     break;
 }
}
?>
</body>
<script src="bootstrap/js/popper.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
