﻿<head>
<script>
$(document).ready(function() {
window.location.href='#bigbang';
});

</script>
<style>
 p{
   text-indent: 20px;
   text-align: justify;
   margin-top: 10px;
   font-size: 18px;
   width: 100%;
   margin-left: 10px;
   margin-right: 50px;
 }
 .fundo_big{
    background-color: white;
 }

</style>
</head>
<div class="container py-5" id="bigbang">
  <br /><br />
  <div class="row justify-content-center fundo_big">

    <div class="col-md-5">
        <br /><br />
        <div class="zoom">
          <img src="img/bigbang.jpg" class="img-fluid"  />
        </div>
        <br /><br /><br />
        <img src="img/bigbang.png" class="img-fluid" />
    </div>
    <div class="col-md-6 py-2">
      <p>
        Big Bang (hangul: 빅뱅, estilizado como BIGBANG) é um grupo masculino sul-coreano
        formado pela YG Entertainment, tendo sua estreia oficial datada em 19 de agosto
        de 2006. Seus membros são G-Dragon, T.O.P, Taeyang e Daesung, em 11 de março de
        2019, o membro Seungri retirou-se da indústria musical. O grupo com suas
        raízes predominantemente no hip hop, obteve inicialmente um sucesso moderado.
        Em 2007, conquistou seu primeiro grande êxito através do lançamento de seu
        primeiro extended play (EP), Always, que produziu a canção "Lies"
        (hangul: 거짓말; rr: Geojitmal). Ela permaneceu na primeira colocação das
        principais paradas musicais sul-coreanas durante seis semanas consecutivas,
        estabelecendo um recorde[8] e venceu o prêmio de Canção do Ano no Mnet Korean
        Music Festival no mesmo ano.
        Seus EPs subsequentes apoiaram o crescente sucesso e popularidade do grupo,
        gerando canções que lideraram as paradas musicais, como "Last Farewell"
        (hangul: 마지막 인사; rr: Majimak Insa) (2007), "Day by Day" (hangul: 하루하루;
        rr: Haru Haru) (2008) e "Sunset Glow" (hangul: 붉은노을; rr: Byulkeun Noeul)
        (2009). Após receber diversos prêmios, dentre eles o de Artista do Ano no
        Mnet Asian Music Awards de 2008, o Big Bang continuou a sua já iniciada expansão
        para o mercado fonográfico japonês, lançando seu primeiro álbum de estúdio no
        país, intitulado Number 1 (2008), desde então, lançou mais quatro álbuns de
        estúdio japoneses: Big Bang (2009), Big Bang 2 (2011), Alive (2012) e Made
        Series (2016), que receberam certificação de ouro pela Recording Industry
        Association of Japan (RIAJ).
        </p>
   </div>
  </div>
</div>
