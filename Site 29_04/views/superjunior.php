﻿<head>
<script>

$(document).ready(function() {
window.location.href='#superjunior';
});
</script>
<style>
 p{
   text-indent: 20px;
   text-align: justify;
   margin-top: 10px;
   font-size: 18px;
   width: 100%;
   margin-left: 10px;
   margin-right: 50px;
 }
 .fundo_super{
    background-color: white;
 }

</style>
</head>
<div class="container py-5" id="superjunior">
  <br /><br />
  <div class="row justify-content-center fundo_super">

    <div class="col-md-5">
        <br /><br />
        <img src="img/superjunior.jpg" class="img-fluid"  />
        <br /><br /><br />
        <img src="img/superjunior1.jpg" class="img-fluid" />
    </div>
    <div class="col-md-6 py-2">
      <p>
          Super Junior (hangul: 슈퍼주니어; rr: Syupeo Junieo) é um grupo masculino
          sul-coreano produzido por Lee Soo-man e formado pela empresa e gravadora S.M.
          Entertainment. Ele estreou, inicialmente, com doze integrantes, em 6 de novembro de 2005,[1] no programa musical Inkigayo, com o single "Twins (Knock Out)".[2] No dia 23 de maio de 2006, a S.M. Entertainment anunciou a adição de um novo integrante, Kyuhyun.[3] [4] Os 13 integrantes originais, que formaram o grupo em seu auge, são Leeteuk (líder), Heechul, Han Geng (ex-integrante), Yesung, Kangin, Shindong, Sungmin, Eunhyuk, Donghae, Siwon, Ryeowook, Kibum (ex-integrante) e Kyuhyun.
          Há ainda Zhou Mi e Henry, parte apenas do subgrupo Super
          Junior-M. Super Junior também é conhecido como SuJu ou SJ, e ainda como
          "Os Reis da Onda Hallyu".
          Com oito álbuns de estúdio lançados na Coreia do Sul, o grupo vendeu mais de 1,7
          milhão de cópias físicas apenas nos últimos cinco anos, com seus trabalhos mais
          recentes. Apesar de ter obtido grande sucesso nacional desde sua estreia, o grupo
          tornou-se internacionalmente conhecido após lançamento de seu hit single "Sorry,
          Sorry", em 2009, do álbum homônimo, que se transformou em um êxito de vendas na
          Coreia e em diversos países asiáticos. O grupo já lançou e contribuiu para mais
          de 20 álbuns, sendo o artista de K-pop que mais vendeu por três anos consecutivos.
          Ao longo dos anos, Super Junior foi dividido em pequenos grupos diferentes,
          visando atingir um número maior de público e estilos musicais diferenciados.
           Devido ao sucesso do Super Junior como artistas, outras empresas de entretenimento
           sul-coreanas começaram a treinar seus grupos musicais em outras áreas, como a
           atuação e a apresentação de programas televisivos.
        </p>
   </div>
  </div>
</div>
